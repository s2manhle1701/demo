using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status_enemy : MonoBehaviour
{
    public GameObject Player;
    public GameObject Win_floor;
    public GameObject Area_normal_attack;
    public GameObject Area_skill_1;
    public GameObject Area_skill_2;
    public GameObject Area_skill_3;
    public Transform P_normal_attack;
    public Transform P_skill_1;
    public Transform P_skill_2;
    public Transform P_skill_3;
    public Slider Health;
    public Slider SL_armor;
    public float Damage;
    public float Armor;
    public float Max_health;

    Effect EF_character;
    Status_character SC;

    void Awake()
    {
        Win_floors WF = Win_floor.gameObject.GetComponent<Win_floors>();
        EF_character = Player.gameObject.GetComponent<Effect>();
        SL_armor.maxValue = Max_health;
        if(WF.Floor >= 1)
        {
            SL_armor.value = Armor;
        }
        Health.maxValue = Max_health;
        Health.value = Max_health;
        SC = Player.gameObject.GetComponent<Status_character>();

    }

    public void Add_damage()
    {
        if(SL_armor.value != 0) SL_armor.value -= SC.Damage;
        else Health.value -= SC.Damage;
        Debug.Log("Add attack :" + SC.Damage);
    }
    public void Add_skill_1()
    {
        float x = EF_character.Damage_skill(1);
        if(SL_armor.value != 0) SL_armor.value -=x;
        else Health.value -= x;
    }
    public void Add_skill_2()
    {
        float x = EF_character.Damage_skill(2);
        if(SL_armor.value != 0) SL_armor.value -=x;
        else Health.value -= x;
    }
    public void Add_passive_1()
    {
        if(SL_armor.value != 0) SL_armor.value -=SC.Magic;
        else Health.value -= SC.Magic;
    }
    public void E_Healing(float healing)
    {
        Health.value += healing;
    }
    public void Player_win()
    {
        Win_floor.SetActive(true);
        SC.Win();
        Invoke("Win_delay", 0.55f);
    }
    public void Create_skill_boss_1()
    {
        Area_skill_1.SetActive(true);
    }
    public void Create_skill(bool face, int num_skill)
    {
        if(num_skill == 0)
        {
            if(face) Instantiate(Area_normal_attack, P_normal_attack.position, Quaternion.Euler(new Vector3(0,0,0)));
            else Instantiate(Area_normal_attack, P_normal_attack.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
        else if(num_skill == 1)
        {
            if(face) Instantiate(Area_skill_1, P_skill_1.position, Quaternion.Euler(new Vector3(0,0,0)));
            else Instantiate(Area_skill_1, P_skill_1.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
        else if(num_skill == 2)
        {
            if(face) Instantiate(Area_skill_2, P_skill_2.position, Quaternion.Euler(new Vector3(0,0,0)));
            else Instantiate(Area_skill_2, P_skill_2.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
        else
        {
            if(face) Instantiate(Area_skill_3, P_skill_3.position, Quaternion.Euler(new Vector3(0,0,0)));
            else Instantiate(Area_skill_3, P_skill_3.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
    }
    void Win_delay()
    {
        gameObject.SetActive(false);
    }
}
