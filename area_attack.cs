using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class area_attack : MonoBehaviour
{
    public float time_alive;
    public int Number_object;

    void Start()
    {
        Destroy(gameObject, time_alive);
    } 
    void OnTriggerEnter2D(Collider2D other)
    {
        if(Number_object == 1 && other.gameObject.tag == "Enemy") Destroy(gameObject, 0);
        if(Number_object == 0 && other.gameObject.tag == "Player") Destroy(gameObject, 0);
    } 
}
