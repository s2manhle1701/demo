using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Super_boss : MonoBehaviour
{
    public GameObject Player; 
    public float Jump_height;
    public float Speed;
    public float Num_of_boss;
    public float Deviant;
    public int Next_attack;
    public int Next_skill_1;
    public int Next_skill_2;
    public int Next_skill_3;
    public int Time_attack;
    public int Time_skill_1;
    public int Time_skill_2;
    public int Time_skill_3;
    public float Attack_delay;
    public bool Start_move = true;
    public bool Start_attack = false;
    public bool start_normal_attack = false;

    Rigidbody2D body;
    Status_enemy SE;
    Effect EF_player;
    Animator ani;

    float health = 1;
    float distance;
    float time_attack_delay = 0;
    float time_stun;
    bool stun = false;
    bool face_right = true;
    bool skill_1 = false;
    bool skill_2 = false;
    bool skill_3 = false;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        SE = GetComponent<Status_enemy>();
        EF_player = Player.gameObject.GetComponent<Effect>();
        Check_num();
    }

    void Update()
    {
        distance = Player.transform.position.x - transform.position.x;
        if(Start_move)
        {
            Move_move_move();
        }
        else if(Start_attack)
        {
            if(Time.time >= Next_attack)
            {
                ATK();
            }
            else if(skill_1 && Time.time >= Next_skill_1)
            {
                Skill_1();
            }
            else if(skill_2 && Time.time >= Next_skill_2)
            {
                Skill_2();
            }
            else if(skill_3 && Time.time >= Next_skill_3)
            {
                Skill_3();
            }
        }
        if(time_attack_delay != 0 && Time.time >= time_attack_delay)
        {
            time_attack_delay = 0;
        }
        if(stun && Time.time >= time_stun)
        {
            stun = false;
        }
        if(health <= 0)
        {
            ani.SetBool("Death", true);
            SE.Player_win();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "ATK")
        {
            ani.SetTrigger("Take hit");
            SE.Add_damage();
            health = SE.Health.value;
            Debug.Log("Collide");
        }
        if(other.gameObject.tag == "PS_1")
        {
            ani.SetTrigger("Take hit");
            SE.Add_passive_1();
            health = SE.Health.value;
        }
        if(other.gameObject.tag == "Skill_1")
        {
            ani.SetTrigger("Take hit");
            SE.Add_skill_1();
            health = SE.Health.value;
            time_stun = (int)Time.time + EF_player.Time_stun;
            stun = true;
            body.drag = Speed;
        }
        if(other.gameObject.tag == "Skill_2")
        {
            ani.SetTrigger("Take hit");
            SE.Add_skill_2();
            health = SE.Health.value;
        }
    }
    void Move_move_move()
    {  
        if(stun || time_attack_delay != 0) return;
        else
        {
            ani.SetBool("Grounded", true);
            Flip();
            if(distance > 0)
            {
                ani.SetInteger("Move", 1);
                body.velocity = new Vector2(Speed, body.velocity.y);
            }
            else if(distance < 0)
            {
                ani.SetInteger("Move", 1);
                body.velocity = new Vector2(-Speed, body.velocity.y);
            }
        }
    }
    void Flip()
    {
        Vector3 flipX = transform.localScale;
        if((face_right && distance > 0) || (!face_right && distance < 0)) return;
        else
        {
            flipX.x *= -1;
            transform.localScale = flipX;
            float x = transform.position.x;
            face_right = !face_right;
            if(distance > 0)
            {
                Vector3 changeX = transform.position;
                changeX.x += Deviant; 
                transform.position = changeX;
            }
            else 
            {
                Vector3 changeX = transform.position;
                changeX.x -= Deviant; 
                transform.position = changeX;
            }
        }
    }
    void ATK()
    {
        if(stun || time_attack_delay != 0) return;
        Flip();
        ani.SetTrigger("Normal attack");
        Next_attack = (int)Time.time + Time_attack;
        SE.Create_skill(face_right, 0);
        time_attack_delay = Time.time + 1.5f;
    }
    void Skill_1()
    {
        if(stun || time_attack_delay != 0) return;
        Flip();
        ani.SetTrigger("Skill 1");
        Next_skill_1 = (int)Time.time + Time_skill_1;
        if(Num_of_boss == 1)SE.Create_skill_boss_1();
        else SE.Create_skill(face_right, 1);
        time_attack_delay = Time.time + 1.5f;
    }
    void Skill_2()
    {
        if(stun || time_attack_delay != 0) return;
        Flip();
        ani.SetTrigger("Skill 2");
        Next_skill_2 = (int)Time.time + Time_skill_2;
        SE.Create_skill(face_right, 2);
        time_attack_delay = Time.time + 1.5f;
    }
    void Skill_3()
    {
        if(stun || time_attack_delay != 0) return;
        Flip();
        ani.SetTrigger("Skill 3");
        Next_skill_3 = (int)Time.time + Time_skill_3;
        SE.Create_skill(face_right, 3);
        time_attack_delay = Time.time + 1.5f;
    }
    void Check_num()
    {
        if(Num_of_boss == 1 || Num_of_boss == 2)skill_1 = true;
        else if(Num_of_boss == 3 || Num_of_boss == 4)
        {
            skill_1 = true;
            skill_2 = true;
        }
        else
        {
            skill_1 = true;
            skill_2 = true;
            skill_3 = true;
        }
    }
}
