using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_controll : MonoBehaviour
{
    public float Speed;
    public float Jump;
    public Transform P_attack;
    public Transform P_Skill_passive;
    public GameObject Area_attack;
    public GameObject Area_skill_1;
    public GameObject Area_skill_2;
    public GameObject Passive_1;
    public float Next_attack = 0;
    public float Next_skill_1 = 0;
    public float Next_skill_2 = 0;
    public float time_passive_1;

    Status_character SC;
    Rigidbody2D body;
    Animator ani;
    Effect EF_player;

    bool  start_jump, face_right, death = false;
    float move, health = 10, mana, time_delay,time_take_hit, take_hit_delay, time_healing = 0; 

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        SC = GetComponent<Status_character>();
        EF_player = GetComponent<Effect>();
        take_hit_delay = EF_player.Get_take_hit_delay();
        face_right = true;
        Invoke("Get_value", 0.04f);
    }

    void Update()
    {
        move = Input.GetAxis("Horizontal");
        if(move > 0.00001 || move < -0.00001) Move_move_move();
        else ani.SetInteger("Move", 0);
        if(Input.GetKeyDown(KeyCode.J))
        {
            ATK();
        }
        if(Input.GetKeyDown(KeyCode.W) && start_jump)
        {
            ani.SetTrigger("Jump");
            body.velocity = new Vector2(body.velocity.x, Jump);
            start_jump = false;
        }
        if(Input.GetKeyDown(KeyCode.S) && start_jump)
        {
            ani.SetTrigger("Sit");
        }
        if(Input.GetKeyDown(KeyCode.K))
        {
            Start_skill_1();
        }
         if(Input.GetKeyDown(KeyCode.L))
        {
            Start_skill_2();
        }
        if(time_take_hit != 0 && Time.time  >= time_take_hit)
        {
            if(time_delay == 0)
            {
                ani.SetTrigger("Take hit");
                health = SC.Health.value;
                time_take_hit = 0;
            }
            else
            {
                health = SC.Health.value;
                time_take_hit = 0;
            }
        }
        if(time_delay != 0 && Time.time >= time_delay)
        {
            Speed = body.drag;
            body.drag = 0;
            time_delay = 0;
        }
        if(SC.Passive_1 && Time.time >= time_passive_1)
        {
            Passive1();
            time_passive_1 = Time.time + 5f/SC.Level_passive_1;
        }
        if(SC.Health_recovery != 0 && Time.time > time_healing)
        {
            Healing(SC.Health_recovery);
            time_healing += 4f;
        }
        if(health <= 0) 
        {
            death = true;
            ani.SetTrigger("Death");
            body.drag = Speed;
            Speed = 0;
        }
    }
    void Move_move_move()
    {
        if(time_delay != 0 || death)
        {
            ani.SetInteger("Move", 0);
            return;
        } 
        body.velocity = new Vector2(move*Speed,body.velocity.y);
        ani.SetInteger("Move", 1);
        if(move > 0 && !face_right)
        {
            Flip();
        }
        else if(move < 0 && face_right)
        {
            Flip();
        }
    }
    void Flip()
    {
        Vector3 flipX = transform.localScale;
        flipX.x *= -1;
        transform.localScale = flipX;
        face_right = !face_right;
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Ground")
        {
            start_jump = true;
            ani.SetBool("Grounded",start_jump);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "E_ATK")
        {
            SC.Add_damage();
            time_take_hit = Time.time + take_hit_delay;
        }
        if(other.gameObject.tag == "E_skill_1")
        {
            SC.Add_skill_1();
            time_take_hit = Time.time + take_hit_delay;
        } 
    }
    void ATK()
    {
        if(mana == 0 || time_delay != 0) return;
        if(Time.time >= Next_attack)
        {
            body.drag = Speed;
            Speed = 0;
            SC.Reduce_mana(1);
            mana = SC.Mana.value;
            ani.SetTrigger("Normal attack");
            if(SC.Life_steal != 0) Healing(SC.Life_steal);
            if(face_right)
            {
                Instantiate(Area_attack, P_attack.position, Quaternion.Euler(new Vector3(0,0,0)));
            }
            else if(!face_right)
            {
                Instantiate(Area_attack, P_attack.position, Quaternion.Euler(new Vector3(0,0,180)));
            }
            EF_player.Get_cooldown(Time.time, 0);
            time_delay = Time.time + 1f;
            Debug.Log("Player attack");
        }
    }
    void Passive1()
    {
        Passive1 PS1 = Passive_1.gameObject.GetComponent<Passive1>();
        PS1.Face_right = face_right;
        if(face_right)
        {
            Instantiate(Passive_1, P_Skill_passive.position, Quaternion.Euler(new Vector3(0,0,0)));
        }
        else if(!face_right)
        {
            Instantiate(Passive_1, P_Skill_passive.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
    }
    void Start_skill_1()
    {
        if(time_delay != 0 || Time.time < Next_skill_1) return;
        ani.SetTrigger("Skill 1");
        body.drag = Speed;
        Speed = 0;
        if(face_right)
        {
            Instantiate(Area_skill_1, P_attack.position, Quaternion.Euler(new Vector3(0,0,0)));
        }
        else if(!face_right)
        {
            Instantiate(Area_skill_1, P_attack.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
        EF_player.Get_cooldown(Time.time, 1);
        time_delay = Time.time + 0.5f;
    }
    void Start_skill_2()
    {
        if(time_delay != 0 || Time.time < Next_skill_2) return;
        Effect_skill_2();
        body.drag = Speed;
        Speed = 0;
        if(face_right)
        {
            Instantiate(Area_skill_2, P_Skill_passive.position, Quaternion.Euler(new Vector3(0,0,0)));
        }
        else if(!face_right)
        {
            Instantiate(Area_skill_2, P_Skill_passive.position, Quaternion.Euler(new Vector3(0,0,180)));
        }
        time_delay = Time.time + 1f;
    }
    void Effect_skill_2()
    {
        if(SC.Level_skill_2 == 1)
        {
            SC.Health.value /= 2;
            health = SC.Health.value;
        }
        else
        {
            SC.Health.value /= 2;
            SC.Health.value += 2;
            health = SC.Health.value;
        }
        EF_player.Get_cooldown(Time.time, 2);
    }
    void Healing(float HP)
    {
        if(HP != 0)
        {
            if(health == SC.Max_health && SC.Create_armor)
            {
                SC.Create_shield(HP);
                return;
            }
            SC.Health.value += HP;
            health = SC.Health.value;
        }
        else return;
    }
    void Mana_Recovery()
    {
        if(SC.Mana_recovery != 0)
        {
            SC.Mana.value += SC.Mana_recovery;
            mana = SC.Mana.value;
        }
    }
    void Get_value()
    {
        health = SC.Max_health;
        mana = SC.Max_mana;
    }
}
