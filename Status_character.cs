using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Status_character : MonoBehaviour
{
    public Slider Health;
    public Slider Mana;
    public Slider SL_armor;
    public GameObject Enemy;
    public GameObject Menu;
    public float Max_health;
    public int Max_mana;
    public float Damage;
    public float Magic;
    public int Health_recovery;
    public int Mana_recovery;
    public int Level_skill_1;
    public int Level_skill_2;
    public int Level_passive_1;
    public int Level_passive_2;
    public float Life_steal;
    public float Armor;
    public int Magic_resist;
    public bool Bool_win_floor = false;
    public bool Create_armor;
    public bool Skill_1;
    public bool Skill_2;
    public bool Passive_1;
    public bool Passive_2;

    Status_enemy SE;

    void Awake()
    {
        SE = Enemy.gameObject.GetComponent<Status_enemy>();
        Invoke("Get_value", 0.02f);
    }
    void Get_value()
    {
        Health.maxValue = Max_health;
        Health.value = Max_health;
        Mana.maxValue = Max_mana;
        Mana.value = Max_mana;
        SL_armor.maxValue = Max_health;
        if(Armor != 0) SL_armor.value = Armor;
        if(Passive_2)
        {
            Create_shield(SE.Armor);
        }
    }
    public void Create_shield(float armor)
    {
        if(SL_armor.value > 0) SL_armor.value += armor;
        else SL_armor.value = armor;
    }
    public void Add_damage()
    {
        if(SL_armor.value != 0) SL_armor.value -= SE.Damage;
        else Health.value -= SE.Damage;
    }
    public void Reduce_mana(int mana_skill)
    {
        if(Mana.value == 0) return;
        Mana.value -= mana_skill;
    }
    public void Add_skill_1()
    {
        if(SL_armor.value != 0) SL_armor.value -= SE.Damage*4;
        else Health.value -= SE.Damage*4;
    }
    public void Win()
    {
        Invoke("Win_delay", 1f);
        Bool_win_floor = true;
    }
    void Win_delay()
    {
        gameObject.SetActive(false);
        Bool_win_floor = true;
    }
    public void Save_status()
    {
        int x;
        PlayerPrefs.SetFloat("Damage", Damage);
        PlayerPrefs.SetFloat("Health", Max_health);
        PlayerPrefs.SetFloat("Life_Steal", Life_steal);
        PlayerPrefs.SetFloat("Armor", Armor);
        PlayerPrefs.SetFloat("Magic", Magic);
        PlayerPrefs.SetInt("Level skill 1", Level_skill_1);
        PlayerPrefs.SetInt("Level skill 2", Level_skill_2);
        PlayerPrefs.SetInt("Level passive 1", Level_passive_1);
        PlayerPrefs.SetInt("Level passive 2", Level_passive_2);
        PlayerPrefs.SetInt("Mana", Max_mana);
        PlayerPrefs.SetInt("Magic_Resist", Magic_resist);
        PlayerPrefs.SetInt("Health_Recovery", Health_recovery);
        PlayerPrefs.SetInt("Mana_Recovery", Mana_recovery);
        if(Skill_1)x = 1;
        else x = 0;
        PlayerPrefs.SetInt("Skill_1", x);
        if(Skill_2)x = 1;
        else x = 0;
        PlayerPrefs.SetInt("Skill_2", x);
        if(Passive_1)x = 1;
        else x = 0;
        PlayerPrefs.SetInt("Passive_1", x);
        if(Passive_2)x = 1;
        else x = 0;
        PlayerPrefs.SetInt("Passive_2", x);
        if(Create_armor)x = 1;
        else x = 0;
        PlayerPrefs.SetInt("Create_Armor", x);
        Debug.Log("co load");
        Win_floors WF = Menu.gameObject.GetComponent<Win_floors>();
        int y = WF.Floor + 1;
        if(y -1 == 5)SceneManager.LoadScene(1);
        else SceneManager.LoadScene(y);
    }
    public void Get_status()
    {
        int x;
        Damage = PlayerPrefs.GetFloat("Damage");
        Max_health = PlayerPrefs.GetFloat("Health");
        Armor = PlayerPrefs.GetFloat("Armor");
        Life_steal = PlayerPrefs.GetFloat("Life_Steal");
        Magic = PlayerPrefs.GetFloat("Magic");
        Level_skill_1 = PlayerPrefs.GetInt("Level skill 1");
        Level_skill_2 = PlayerPrefs.GetInt("Level skill 2");
        Level_passive_1 = PlayerPrefs.GetInt("Level passive 1");
        Level_passive_2 = PlayerPrefs.GetInt("Level passive 2");
        Max_mana = PlayerPrefs.GetInt("Mana");
        Magic_resist = PlayerPrefs.GetInt("Magic_Resist");
        Health_recovery = PlayerPrefs.GetInt("Health_Recovery");
        Mana_recovery = PlayerPrefs.GetInt("Mana_Recovery");
        x = PlayerPrefs.GetInt("Skill_1");
        if(x == 1) Skill_1 = true;
        else Skill_1 = false;
        x = PlayerPrefs.GetInt("Skill_2");
        if(x == 1) Skill_2 = true;
        else Skill_2 = false;
        x = PlayerPrefs.GetInt("Passive_1");
        if(x == 1) Passive_1 = true;
        else Passive_1 = false;
        x = PlayerPrefs.GetInt("Passive_2");
        if(x == 1) Passive_2 = true;
        else Passive_2 = false;
        x = PlayerPrefs.GetInt("Create_Armor");
        if(x == 1) Create_armor = true;
        else Create_armor = false;

    }
    /*public void First_save()
    {
        PlayerPrefs.SetFloat("Damage", Damage);
        PlayerPrefs.SetFloat("Health", Max_health);
        PlayerPrefs.SetFloat("Life_Steal", Life_steal);
        PlayerPrefs.SetFloat("Armor", Armor);
        PlayerPrefs.SetFloat("Magic", Magic);
        PlayerPrefs.SetInt("Level skill 1", Level_skill_1);
        PlayerPrefs.SetInt("Level skill 2", Level_skill_2);
        PlayerPrefs.SetInt("Level passive 1", Level_passive_1);
        PlayerPrefs.SetInt("Level passive 2", Level_passive_2);
        PlayerPrefs.SetInt("Mana", Max_mana);
        PlayerPrefs.SetInt("Magic_Resist", Magic_resist);
        PlayerPrefs.SetInt("Health_Recovery", Health_recovery);
        PlayerPrefs.SetInt("Mana_Recovery", Mana_recovery);
        PlayerPrefs.SetInt("Skill_1", x);
        PlayerPrefs.SetInt("Skill_2", x);
        PlayerPrefs.SetInt("Passive_1", x);
        PlayerPrefs.SetInt("Passive_2", x);
        PlayerPrefs.SetInt("Create_Armor", x);
    }*/
    public void Delete_save()
    {
        PlayerPrefs.DeleteAll();
    }
}
